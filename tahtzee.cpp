/*!
 *  Copyright (C) 2015 Cursive D Software LLC, All Rights Reserved
 *
 *  Authors: Darren M. OConnell
 *
 *  Tahtzee Trunk Build
 */

 #include "headers/tahtzee.h"
 #include "headers/play.h"

Tahtzee::Tahtzee()
{
  playing=true;


}
Tahtzee::~Tahtzee()
{

}

 void Tahtzee::about()
 {
   cout<<"about()"<<endl;
   //!TODO: Insert About Game Development Here
   /*!
   * This is a terminal based version of yahzee.
   */
 }

 void Tahtzee::initGame()
 {

   cout<<"initGame()"<<endl;
   //!TODO: Insert Inital Game Logic Here
   int quitting=0;
   while(playing==true)
   {
     testcall();
     cout<<"Keep Playing?(1)Yes or (2)No ";
     cin>>quitting;
     if(quitting==2)
     {
       playing=false;
       exit(0);
     }

   }

 }

 void Tahtzee::help()
 {
   cout<<"help()"<<endl;
   //!TODO: Insert Help Information Here
 }

 void Tahtzee::quit()
 {
   cout<<"quit()"<<endl;
   //!TODO: Insert Save Game Code Here
   exit(0);
 }
 void Tahtzee::testcall()
 {
   //!This is to test gameplay calls
   p.rolldice();
   //p.storedice();
   p.card();
 }
