/*!
 *  Copyright (C) 2015 Cursive D Software LLC, All Rights Reserved
 *
 *  Authors: Darren M. OConnell
 *
 *  Tahtzee Trunk Build
 */
 #include <iostream>
 #include <cstdlib>
 #include <ctime>
 using namespace std;

 #ifndef PLAY_H_DMO
 #define PLAY_H_DMO
 class Play{
   static const int _DEFAULT=0;
   static const int _BONUS=35;
   static const int _FULLHOUSE=25;
   static const int _SMALLSTRIGHT=30;
   static const int _LARGESTRIGHT=40;
   static const int _TAHTZEE=50;
   static const int _TAHTZEEBONUS=100;
   static const int _DICEMAX=6;
   static const int _MAXROLL=3;
   static const bool _DEFAULTBOOL=false;
   int Aces,Twos,Threes,Fours,Fives,Sixes,Chance;
   int TahtzeeBonusCount;
   int lowerTotal;
   int upperTotal;
   int grandTotal;
   int dice[_DICEMAX];
   int diceStore[_DICEMAX];
   bool BonusRec;
   bool FullHouseRec;
   bool SmallStrightRec;
   bool LargeStrightRec;
   bool TahtzeeRec;




 public:

   Play();
   ~Play();
   void rolldice();
   void storedice();
   void holddice();
   void card();


 };
 #endif
