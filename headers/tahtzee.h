/*!
 *  Copyright (C) 2015 Cursive D Software LLC, All Rights Reserved
 *
 *  Authors: Darren M. OConnell
 *
 *  Tahtzee Trunk Build
 */
#include <iostream>
#include <cstdlib>
#include "play.h"
 using namespace std;

#ifndef TAHTZEE_H_DMO
#define TAHTZEE_H_DMO

class Tahtzee{
  Play p;
  bool playing;

public:

  Tahtzee();
  ~Tahtzee();
  void about();
  void initGame();
  void help();
  void quit();
  void testcall(); /*! Testing to see if play works*/



};
#endif
