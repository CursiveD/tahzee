/*!
 *  Copyright (C) 2015 Cursive D Software LLC, All Rights Reserved
 *
 *  Authors: Darren M. OConnell
 *
 *  Tahtzee Trunk Build
 */
#include <iostream>
#include <cstdlib>
#include "headers/tahtzee.h"
#include "headers/play.h"

using namespace std;



void menu()
{
  Tahtzee y;
  while(1>0)
  {
    int userSelection;

    cout<<"What Would You Like To Do? \n(1)New Game, (2)About, (3)Help, (4)Exit\n>> ";
    cin>>userSelection;

    if(userSelection == 1)
    {
      y.initGame();
    }
    if(userSelection == 2)
    {
      y.about();
    }
    if(userSelection == 3)
    {
      y.help();
    }
    if(userSelection == 4)
    {
      y.quit();
    }


  }


}

int main()
{
    cout<<"Welcome to Tahtzee"<<endl;
    menu();

}
