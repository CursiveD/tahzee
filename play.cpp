/*!
 *  Copyright (C) 2015 Cursive D Software LLC, All Rights Reserved
 *
 *  Authors: Darren M. OConnell
 *
 *  Tahtzee Trunk Build
 */
#include "headers/play.h"

Play::Play()
{
  /* Initalize ScoreCard */
  Aces=Twos=Threes=Fours=Fives=Sixes=Chance=TahtzeeBonusCount=lowerTotal=upperTotal=grandTotal=_DEFAULT;

  BonusRec=FullHouseRec=SmallStrightRec=LargeStrightRec=TahtzeeRec=_DEFAULTBOOL;

  /* Initalize Dice   */
  for(int i=0; i<_DICEMAX; i++)
  {
    dice[i]=0;
  }
  srand (time(NULL));

  /* Initalize diceStore*/
  for(int i=0; i<_DICEMAX; i++)
  {
    diceStore[i]=0;
  }

}
Play::~Play()
{

}
void Play::rolldice()
{
 for(int j=0; j<_MAXROLL; j++)
 {
     for(int i=0; i<_DICEMAX; i++)
     {
       dice[i]= rand() % 6 + 1;
       cout<<dice[i]<<"|";
     }
	storedice();
	cout<<endl;
 }
 cout<<endl;

}
void Play::storedice()
{
  int storeAny=0;
  //! TODO: Insert Store Dice Logic
  cout<<"\nWould you like to store any?(1)Yes or (2)No";
  cin>>storeAny;
  if(storeAny==1)
  {
      //! TODO: Find out which dice are to be stored and store them.
  }

  cout<<"storedice()"<<endl;
  cin.ignore();
}
void Play::holddice()
{
  //! TODO: Insert Hold Dice Logic
  cout<<"holddice()"<<endl;
}
void Play::card()
{
  //! TODO: Insert Tahzee Card Logic
  cout<<"card()"<<endl;
}
