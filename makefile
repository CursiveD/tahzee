tahtzee.dmo: main.o tahtzee.o play.o
	g++ -pedantic main.o tahtzee.o play.o -o bin/tahtzee.dmo
main.o: main.cpp
	g++ -pedantic -Wall main.cpp -c
tahtzee.o: headers/tahtzee.h tahtzee.cpp play.o
	g++ -pedantic -Wall tahtzee.cpp -c
play.o: headers/play.h play.cpp
	g++ -pedantic -Wall play.cpp -c
clean:
		rm *.o bin/*.dmo
